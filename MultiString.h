/*!
 * file: MultiString.h
 * MultiString class definition
 */
#pragma once

class MultiString {
public:
    //constructors destructor
    MultiString();

    MultiString(int);

    ~MultiString();

    //methods
    char *operator[](int nindex) const;

    MultiString &operator+=(const MultiString &);

    MultiString MergeMultiStringexclusive(const MultiString &);

    int Find(const char *pszsub) const;

    int Getlength() const;

    bool Isempty() const;

    void Empty();

    void Setat(int nindex, const char *str);

    void Printstr(int nindex) const;

private:
    //forbidden copy constructor
    MultiString(const MultiString &ms);

    //attributes
    char **buf;//pointer to vector
    int str_nmb;//strings number
};

