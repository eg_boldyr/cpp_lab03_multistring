#include <stdio.h>
//#include <conio.h>
#include <curses.h>
#include <iostream>
#include "MultiString.h"

int main() {

    int n;
    char str[127];
    std::cout << "Input size of string vectors: ";
    std::cin >> n;

    MultiString s(n), s2(n);

    if (!s.Isempty()) {
        std::cout << "\nInput lines of first vector:\n";

        for (int i = 0; i < n; i++) {
            std::cout << "\nInput line #" << i + 1 << "\t";
            std::cin.sync();
            std::cin.getline(str, 127);
            std::cin.sync();
            s.Setat(i, str);
        }

        std::cout << "\n\nLines of first vector: ";
        for (int i = 0; i < s.Getlength(); i++) {
            s.Printstr(i);
        }

        std::cout << "\n\nInput lines of second vector:\n";

        for (int i = 0; i < n; i++) {
            std::cout << "\nInput line #" << i + 1 << "\t";
            std::cin.sync();
            std::cin.getline(str, 127);
            std::cin.sync();
            s2.Setat(i, str);
        }

        std::cout << "\n\nLines of second vector: ";
        for (int i = 0; i < s2.Getlength(); i++) {
            s2.Printstr(i);
        }

        std::cout << "\n\nResult of Concatenation: ";

        MultiString s3(s.Getlength());
        for (int i = 0; i < s3.Getlength(); i++) {
            s3.Setat(i, s[i]);
        }
        s3 += s2;

        for (int i = 0; i < s3.Getlength(); i++) {
            s3.Printstr(i);
        }
        std::cout << "\nSize of concatenated vector: " << s3.Getlength();

        std::cout << "\n\nResult of Merge the exclusive strings : ";
        s3 = s.MergeMultiStringexclusive(s2);
        for (int i = 0; i < s3.Getlength(); i++) {
            s3.Printstr(i);
        }
        std::cout << "\nSize of merged vector: " << s3.Getlength();

        std::cout << "\n\nInput string for Find in first vector: ";
        std::cin.sync();
        std::cin.getline(str, 127);
        std::cin.sync();
        std::cout << "\n\nIndex of string : " << s.Find(str);

        std::cout << "\n\nDo you want to clear vectors? (y/n) ";
        char c;
        std::cin >> c;
        if (c == 'y') {
            s.Empty();
            s2.Empty();
            s3.Empty();
        }
        std::cout << "\nResult of check first vector for empty is " << s.Isempty();
        std::cout << "\nResult of check second vector for empty is " << s2.Isempty();
        std::cout << "\nResult of check temporary vector for empty is " << s3.Isempty();
        s3.~MultiString();
    } else {
        std::cout << "\nNo operations are available: Vectors is Empty";
    }

    s.~MultiString();
    s2.~MultiString();

    getch();
    return 0;
}

