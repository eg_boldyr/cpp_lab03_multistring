#include <iostream>
#include <cstring>
#include "MultiString.h"

MultiString::MultiString() {
    str_nmb = 0;
    buf = new char *;
    buf = NULL;
}


MultiString::MultiString(int n) {
    str_nmb = n;
    buf = new char *[str_nmb];
    for (int i = 0; i < str_nmb; i++) {
        buf[i] = new char[127];
    }
}

MultiString::MultiString(const MultiString &ms) {
    this->str_nmb = ms.str_nmb;
    buf = new char *[str_nmb];
    for (int i = 0; i < str_nmb; i++) {
        buf[i] = new char[127];
    }
    for (int i = 0; i < this->str_nmb; i++) {
        strcpy(this->buf[i], ms.buf[i]);
    }
}

MultiString::~MultiString() {
    for (int i = 0; i < str_nmb; i++) {
        delete[] buf[i];
    }
    delete[] buf;
}

char *MultiString::operator[](int nindex) const {
    MultiString copystr(*this);
    return copystr.buf[nindex];
}

MultiString &MultiString::operator+=(const MultiString &src) {

    for (int i = 0; i < str_nmb; i++) {
        strcat(this->buf[i], src.buf[i]);
    }
}

MultiString MultiString::MergeMultiStringexclusive(const MultiString &src) {

    int newnmb = this->str_nmb + src.str_nmb, fn = 0, fn1 = 0;
    MultiString ms(newnmb);
    bool flag = true;

    for (int i = 0; i < this->str_nmb; i++) {
        flag = false;
        for (int j = 0; j < i; j++) {
            if (!strcmp(this->buf[i], ms.buf[j])) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            strcpy(ms.buf[fn], this->buf[i]);
            fn++;
        }
    }
    fn1 = fn;

    for (int i = fn1; i < fn1 + src.str_nmb; i++) {
        flag = false;
        for (int j = 0; j < i; j++) {
            if (!strcmp(src.buf[i - fn1], ms.buf[j])) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            strcpy(ms.buf[fn], src.buf[i - fn1]);
            fn++;
        }
    }
    ms.str_nmb = fn;

    return ms;
}


int MultiString::Find(const char *pszsub) const {
    int num = -1;
    for (int i = 0; i < str_nmb; i++) {
        if (!strcmp(buf[i], pszsub)) {
            num = i;
            break;
        }
    }
    return num;
}


int MultiString::Getlength() const {
    return this->str_nmb;
}

bool MultiString::Isempty() const {

    return str_nmb == 0;
}

void MultiString::Empty() {

    for (int i = 0; i < str_nmb; i++) {
        buf[i] = NULL;
    }
    str_nmb = 0;
}

void MultiString::Setat(int nindex, const char *str) {
    strcpy(buf[nindex], str);
}

void MultiString::Printstr(int nindex) const {
    std::cout << "\n" << buf[nindex];
}
